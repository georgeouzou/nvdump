use ash::extensions::*;
use ash::prelude::VkResult;
use ash::vk;
use clap::Parser;
use spirv_reflect::types::descriptor::ReflectDescriptorType;
use spirv_reflect::ShaderModule;
use std::ffi::CString;
use std::fs::File;
use std::io;
use std::io::{Read, Write};
use std::path::Path;
use std::process::Command;
use tempfile::tempdir;

/// Simple program to greet a person
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// SPIR-V file name
    spirv: String,

    /// Vulkan physical device index
    #[arg(short, long)]
    device_id: Option<usize>,

    /// Enable update-after-bind on descriptors
    #[arg(long)]
    update_after_bind: bool,

    /// Entrypoint name
    #[arg(short, long)]
    entry_point: Option<String>,
}

pub fn dump_data(data: &[u8]) {
    for i in 0..(data.len() / 4) {
        let dw: [u8; 4] = if (i + 1) * 4 <= data.len() {
            data[(i * 4)..(i * 4 + 4)].try_into().unwrap()
        } else {
            let mut partial = Vec::from(&data[(i * 4)..]);
            partial.resize(4, 0);
            partial.try_into().unwrap()
        };

        if i % 8 == 0 {
            print!("\n ");
        }
        print!(" {:08x}", u32::from_le_bytes(dw));
    }
    print!("\n");
}

pub fn find_zstd_header(bin: &Vec<u8>) -> Option<usize> {
    if bin.len() < 4 {
        return None;
    }

    let header = 0xfd2fb528_u32.to_ne_bytes();
    for i in 0..(bin.len() - 3) {
        if bin[i..(i + 4)] == header {
            return Some(i);
        }
    }
    None
}

pub fn find_shader_header_offset(bin: &[u8]) -> Option<usize> {
    // the shader header offset is stored in a section that has the following format:
    // 2D 00 00 00 80 00 00 00 XX XX 00 00 00 00 00 00, where the Xs store the offset
    let section_2d = 0x0000002d_u32.to_ne_bytes();
    for i in (0..bin.len()).step_by(16) {
        if bin[i..(i + 4)] == section_2d {
            let bytes = bin[(i+8)..(i+12)].try_into().unwrap();
            let offset = u32::from_ne_bytes(bytes).try_into().unwrap();
            return Some(offset);
        }
    }
    None
}

struct Shader {
    name: String,
    header: Vec<u8>,
    code: Vec<u8>,
    bin: Vec<u8>
}

impl Shader {
    pub fn from_bin(name: &str, bin: Vec<u8>) -> Self {
        let zstd_start = find_zstd_header(&bin).unwrap();
        let dec = zstd::stream::decode_all(&bin[zstd_start..]).unwrap();

        // the binary that can be disassembled with nvdisasm
        // starts at an offset of 8 bytes
        let all = &dec[8..];
        let hdr_start = find_shader_header_offset(all).unwrap();
        let hdr_end = hdr_start + 128; // turing has a 128 byte header
        let code_start = hdr_end;
        let mut code_end = hdr_end;
        while code_end < all.len() {
            if all[code_end..(code_end + 16)] == [0; 16] {
                break;
            }
            code_end += 16;
        }

        Shader {
            name: name.to_string(),
            header: all[hdr_start..hdr_end].into(),
            code: all[code_start..code_end].into(),
            bin: all.into(),
        }
    }

    pub fn disassemble(&self) {
        let dir = tempdir().expect("Failed to create temp directory");
        let path = dir.path().join("shader.bin");

        {
            let mut file = File::create(&path).expect("Failed to write temp file");
            file.write(&self.bin).expect("Failed to write temp file");
            file.flush().expect("Failed to write temp file");
        }

        let nvdisasm_cmd = if cfg!(unix) {
            "/usr/local/cuda-11.7/bin/nvdisasm"
        } else {
            "nvdisasm"
        };
        Command::new(nvdisasm_cmd)
            .arg("-hex")
            .arg("-b")
            .arg("SM75")
            .arg(path)
            .status()
            .expect("Command failed");
    }

    pub fn dump_header(&self) {
        dump_data(&self.header);
    }

    pub fn dump_code(&self) {
        dump_data(&self.code);
    }
}

fn read_spirv_file(fname: &str) -> Vec<u32> {
    let f = File::open(fname).expect("Failed to open file");
    let mut reader = io::BufReader::new(f);
    let mut buffer = Vec::new();
    reader
        .read_to_end(&mut buffer)
        .expect("Failed to read file");

    let mut spirv = Vec::new();
    assert!(buffer.len() % 4 == 0);
    for i in 0..(buffer.len() / 4) {
        let bytes = buffer[(i * 4)..(i * 4 + 4)].try_into().unwrap();
        spirv.push(u32::from_ne_bytes(bytes));
    }
    spirv
}

fn create_descriptor_set_layouts(
    dev: &ash::Device,
    r_mod: &ShaderModule,
    entry_point: &str,
    stage: vk::ShaderStageFlags,
    args: &Args,
) -> Vec<vk::DescriptorSetLayout> {
    let r_sets = r_mod
        .enumerate_descriptor_sets(Some(entry_point))
        .expect("Failed to enumerate descriptor sets");

    let max_set = r_sets.iter().map(|s| s.set).max().unwrap_or(0);
    let num_sets = usize::try_from(max_set).unwrap() + 1;

    let mut r_set_bindings = Vec::new();
    r_set_bindings.resize_with(num_sets, || Vec::new());
    for r_set in r_sets {
        let set_idx = usize::try_from(r_set.set).unwrap();
        r_set_bindings[set_idx] = r_set.bindings;
    }

    let mut sets = Vec::new();
    for r_bindings in r_set_bindings {
        let mut has_update_after_bind = false;
        let mut binding_flags = Vec::new();
        let mut bindings = Vec::new();
        for r_b in r_bindings {
            if args.update_after_bind {
                let mut flags = vk::DescriptorBindingFlags::empty();
                match r_b.descriptor_type {
                    ReflectDescriptorType::Sampler
                    | ReflectDescriptorType::CombinedImageSampler
                    | ReflectDescriptorType::SampledImage
                    | ReflectDescriptorType::StorageImage => {
                        has_update_after_bind = true;
                        flags |= vk::DescriptorBindingFlags::UPDATE_AFTER_BIND;
                    }
                    _ => (),
                }
                binding_flags.push(flags);
            }

            let desc_type = match r_b.descriptor_type {
                ReflectDescriptorType::Undefined => panic!("Unknown descriptor type"),
                ReflectDescriptorType::Sampler => vk::DescriptorType::SAMPLER,
                ReflectDescriptorType::CombinedImageSampler => {
                    vk::DescriptorType::COMBINED_IMAGE_SAMPLER
                }
                ReflectDescriptorType::SampledImage => vk::DescriptorType::SAMPLED_IMAGE,
                ReflectDescriptorType::StorageImage => vk::DescriptorType::STORAGE_IMAGE,
                ReflectDescriptorType::UniformTexelBuffer => {
                    vk::DescriptorType::UNIFORM_TEXEL_BUFFER
                }
                ReflectDescriptorType::StorageTexelBuffer => {
                    vk::DescriptorType::STORAGE_TEXEL_BUFFER
                }
                ReflectDescriptorType::UniformBuffer => vk::DescriptorType::UNIFORM_BUFFER,
                ReflectDescriptorType::StorageBuffer => vk::DescriptorType::STORAGE_BUFFER,
                ReflectDescriptorType::UniformBufferDynamic => {
                    vk::DescriptorType::UNIFORM_BUFFER_DYNAMIC
                }
                ReflectDescriptorType::StorageBufferDynamic => {
                    vk::DescriptorType::STORAGE_BUFFER_DYNAMIC
                }
                ReflectDescriptorType::InputAttachment => vk::DescriptorType::INPUT_ATTACHMENT,
                ReflectDescriptorType::AccelerationStructureKHR => {
                    vk::DescriptorType::ACCELERATION_STRUCTURE_KHR
                }
            };
            bindings.push(vk::DescriptorSetLayoutBinding {
                binding: r_b.binding,
                descriptor_type: desc_type,
                descriptor_count: r_b.count,
                stage_flags: stage,
                ..Default::default()
            });
        }

        let mut layout_flags = vk::DescriptorSetLayoutBindingFlagsCreateInfo {
            binding_count: binding_flags.len().try_into().unwrap(),
            p_binding_flags: binding_flags.as_ptr(),
            ..Default::default()
        };

        let mut layout_info = vk::DescriptorSetLayoutCreateInfo {
            binding_count: bindings.len().try_into().unwrap(),
            p_bindings: bindings.as_ptr(),
            ..Default::default()
        };
        if has_update_after_bind {
            layout_info.flags |= vk::DescriptorSetLayoutCreateFlags::UPDATE_AFTER_BIND_POOL;
            layout_info.push_next(&mut layout_flags);
        }
        sets.push(unsafe {
            dev.create_descriptor_set_layout(&layout_info, None)
                .expect("Failed to create descriptor set layout")
        });
    }
    sets
}

fn next_stages(stage: vk::ShaderStageFlags) -> vk::ShaderStageFlags {
    if stage == vk::ShaderStageFlags::VERTEX {
        vk::ShaderStageFlags::TESSELLATION_CONTROL
            | vk::ShaderStageFlags::GEOMETRY
            | vk::ShaderStageFlags::FRAGMENT
    } else if stage == vk::ShaderStageFlags::TESSELLATION_CONTROL {
        vk::ShaderStageFlags::TESSELLATION_EVALUATION
    } else if stage == vk::ShaderStageFlags::TESSELLATION_EVALUATION {
        vk::ShaderStageFlags::GEOMETRY | vk::ShaderStageFlags::FRAGMENT
    } else if stage == vk::ShaderStageFlags::GEOMETRY {
        vk::ShaderStageFlags::FRAGMENT
    } else if stage == vk::ShaderStageFlags::FRAGMENT {
        vk::ShaderStageFlags::empty()
    } else if stage == vk::ShaderStageFlags::TASK_EXT {
        vk::ShaderStageFlags::MESH_EXT
    } else if stage == vk::ShaderStageFlags::MESH_EXT {
        vk::ShaderStageFlags::FRAGMENT
    } else {
        panic!("Unsupported shader stage");
    }
}

const VK_VENDOR_ID_NVIDIA: u32 = 0x10de;

unsafe fn find_nv_device(
    inst: &ash::Instance,
    pdevs: Vec<vk::PhysicalDevice>,
) -> Option<vk::PhysicalDevice> {
    for pdev in pdevs {
        let props = inst.get_physical_device_properties(pdev);
        if props.vendor_id == VK_VENDOR_ID_NVIDIA {
            return Some(pdev);
        }
    }
    None
}

unsafe fn create_device(inst: &ash::Instance, args: &Args) -> VkResult<ash::Device> {
    let pdevs = inst.enumerate_physical_devices()?;

    let pdev = if let Some(id) = args.device_id {
        pdevs[id]
    } else {
        find_nv_device(inst, pdevs).expect("Failed to find NVIDIA device")
    };

    let ext_names = [ext::ShaderObject::NAME.as_ptr(), ext::MeshShader::NAME.as_ptr()];
    let mut v12 = vk::PhysicalDeviceVulkan12Features {
        descriptor_binding_sampled_image_update_after_bind: vk::TRUE,
        descriptor_binding_storage_image_update_after_bind: vk::TRUE,
        ..Default::default()
    };
    let mut eso = vk::PhysicalDeviceShaderObjectFeaturesEXT {
        shader_object: vk::TRUE,
        ..Default::default()
    };
    let mut mesh = vk::PhysicalDeviceMeshShaderFeaturesEXT {
        mesh_shader: vk::TRUE,
        ..Default::default()
    };
    let create = vk::DeviceCreateInfo::default()
        .enabled_extension_names(&ext_names)
        .push_next(&mut v12)
        .push_next(&mut eso)
        .push_next(&mut mesh);
    inst.create_device(pdev, &create, None)
}

fn compile_shaders(spirv: &Vec<u32>, args: &Args) -> Vec<Shader> {
    let r_mod = ShaderModule::load_u32_data(spirv).expect("Failed to parse SPIR-V");

    unsafe {
        let entry = ash::Entry::load().expect("Failed to load libvulkan.so");
        let app_info = vk::ApplicationInfo {
            api_version: vk::make_api_version(0, 1, 3, 0),
            ..Default::default()
        };
        let create_info = vk::InstanceCreateInfo {
            p_application_info: &app_info,
            ..Default::default()
        };
        let inst = entry
            .create_instance(&create_info, None)
            .expect("Failed to create Vulkan instance");
        let dev = create_device(&inst, args).expect("Failed to create Vulkan device");
        let eso = ext::ShaderObject::new(&inst, &dev);

        let entrypoints = r_mod
            .enumerate_entry_points()
            .expect("Failed to enumerate SPIR-V entrypoints");

        let mut shaders = Vec::new();
        for e in entrypoints {
            if let Some(arg_ename) = &args.entry_point {
                if &e.name != arg_ename {
                    continue;
                }
            }

            let stage = vk::ShaderStageFlags::from_raw(e.shader_stage.bits());
            let set_layouts = create_descriptor_set_layouts(&dev, &r_mod, &e.name, stage, args);

            let c_name = CString::new(e.name.as_bytes()).unwrap();
            let shader_info = vk::ShaderCreateInfoEXT {
                stage: stage,
                next_stage: next_stages(stage),
                code_type: vk::ShaderCodeTypeEXT::SPIRV,
                code_size: spirv.len() * 4,
                p_code: spirv.as_ptr() as *const _,
                p_name: c_name.as_ptr(),
                set_layout_count: set_layouts.len().try_into().unwrap(),
                p_set_layouts: set_layouts.as_ptr(),
                ..Default::default()
            };
            let shader_objs = eso
                .create_shaders(&[shader_info], None)
                .expect("Failed to compile shaders");
            assert!(shader_objs.len() == 1);

            let bin = eso
                .get_shader_binary_data(shader_objs[0])
                .expect("Failed to retrieve shader binary");

            shaders.push(Shader::from_bin(&e.name, bin));

            eso.destroy_shader(shader_objs[0], None);
            for set in set_layouts {
                dev.destroy_descriptor_set_layout(set, None);
            }
        }

        dev.destroy_device(None);
        inst.destroy_instance(None);

        shaders
    }
}

fn main() {
    let args = Args::parse();

    let spirv = read_spirv_file(&args.spirv);
    let shaders = compile_shaders(&spirv, &args);
    if shaders.is_empty() {
        println!("No shaders found in SPIR-V module");
        return;
    }

    for s in shaders {
        println!("Entrypoint: {}", s.name);
        //s.dump_header();
        //s.dump_code();
        s.disassemble();
    }
}
